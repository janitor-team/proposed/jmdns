Source: jmdns
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: James Page <james.page@ubuntu.com>
Build-Depends:
 debhelper (>= 11),
 default-jdk,
 junit4,
 libeasymock-java,
 libmaven-bundle-plugin-java,
 libmaven-javadoc-plugin-java,
 libslf4j-java,
 maven-debian-helper
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/java-team/jmdns.git
Vcs-Browser: https://salsa.debian.org/java-team/jmdns
Homepage: https://github.com/jmdns/jmdns

Package: libjmdns-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Suggests: ${maven:OptionalDepends}
Description: Java implementation of multi-cast DNS (Apple Rendezvous)
 JmDNS is a Java implementation of the IETF draft RFP multicast extensions for
 DNS.
 .
 This project contains the needed elements for service discovery and service
 registration.
 .
 It is compatible with Apple's Rendezvous service.

Package: libjmdns-java-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: libjmdns-java
Description: Documentation for Java implementation of multi-cast DNS
 JmDNS is a Java implementation of the IETF draft RFP multicast extensions for
 DNS.
 .
 This project contains the needed elements for service discovery and service
 registration.
 .
 It is compatible with Apple's Rendezvous service.
 .
 This package contains the documentation and javadoc for JmDNS.
